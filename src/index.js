import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
// import App from './App';
import $ from "jquery";
// import Map from './Map/Map';
import Dot from './Map/Dot';

import * as serviceWorker from './serviceWorker';

ReactDOM.render(<Dot />, $('.dashboard')[0]);

// ReactDOM.render(<App />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
